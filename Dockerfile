FROM python:3.8.2-alpine

# Set the working directory
WORKDIR /app

# Install Git
RUN apk update && apk add --no-cache git

# Set build arguments for GitLab credentials
ARG GITLAB_USERNAME
ARG GITLAB_PASSWORD

# Clone the repository from GitLab
RUN git clone -b dev https://${GITLAB_USERNAME}:${GITLAB_PASSWORD}@gitlab.com/sela-tracks/1101/students/idansadi/dotnetcore-example.git /app

# Install Python dependencies
RUN pip install --no-cache-dir -r /app/requirements.txt

# Set environment variables
ENV FLASK_APP=mycar.py
EXPOSE 5000

# Set the command to run the Flask application
CMD ["flask", "run", "--host=0.0.0.0", "--port=5000"]
