import pytest
from pymongo import MongoClient
from mycar import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_index_page(client):
    response = client.get('/')
    assert response.status_code == 200
    assert b"Welcome to Car Rental" in response.data

def test_database_connection():
    client = MongoClient('mongodb+srv://idansadi45:ghsixsh010203@cluster0.ajwbubv.mongodb.net/'
    '?retryWrites=true&w=majority&appName=Cluster0')
    db = client['myrentdb']
    assert db.name == 'myrentdb'
    assert 'Cars' in db.list_collection_names()
    assert 'Reservations' in db.list_collection_names()


def test_rent_page(client):
    response = client.get('/rent')
    assert response.status_code == 405  # Method Not Allowed

def test_confirm_rent_page(client):
    response = client.get('/confirm_rent')
    assert response.status_code == 405  # Method Not Allowed

def test_add_driver_page(client):
    response = client.post('/add_driver', json={
        "driver_full_name": "John Doe",
        "driver_license_number": "123456789",
        "driver_age": 30
    })
    assert response.status_code == 200
    assert b"Driver added successfully" in response.data

def test_save_driver_details_page(client):
    response = client.post('/save_driver_details', data={
        "driver_full_name": "John Doe",
        "driver_license_number": "123456789",
        "driver_age": 30
    })
    assert response.status_code == 200
    assert b"Driver details saved successfully" in response.data

def test_invalid_route(client):
    response = client.get('/invalid_route')
    assert response.status_code == 404

# Add more test cases as needed
