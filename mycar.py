import random
import string, os
from flask import Flask, render_template, request, jsonify
from pymongo import MongoClient
from datetime import datetime
from prometheus_flask_exporter import PrometheusMetrics


app = Flask(__name__)
metrics = PrometheusMetrics(app)
# Track HTTP request count and duration by endpoint and status code
metrics.info('app_info', 'Application info', version='1.0.0')
metrics.info('app_debug', 'Application debug')


MONGO_URI = os.environ.get('MONGO_URI', 'mongodb://root:w4JlZr7tVi@mongodb:27017/')

client = MongoClient(MONGO_URI)

db = client.mycar
collection = db['Cars']
rental_collection = db['Reservations']

# Read cars from file
def read_cars_from_file():
    """Read cars from file"""
    cars = []
    with open("cars.txt", "r", encoding="utf-8") as inner_file:
        for inner_line in inner_file:
            inner_details = inner_line.strip().split(",")
            if len(inner_details) == 6:  # Ensure that the line has all expected elements
                car_data_inner = {
                    "brand": inner_details[1],
                    "model": inner_details[2],
                    "year": int(inner_details[3]),
                    "price_per_day": int(inner_details[4]),
                    "type": inner_details[5]
                }
                cars.append(car_data_inner)
            else:
                print("Skipping line due to incorrect format:", inner_line)
    return cars



cars = read_cars_from_file()
car_types = set(car["type"] for car in cars)
car_types = list(car_types)

# Preprocess car_types to organize them into two separate lists
car_types_upper_row = ["Van", "Electric", "SUV"]
car_types_lower_row = ["Family", "Truck"]


@app.route("/")

@metrics.counter('rent_endpoint_requests_total', 'Number of requests to /rent endpoint')
def index():
    """Render the index page."""
    car_types = set(car["type"] for car in cars)
    car_type_images = {
        'Truck': 'https://norfolktruckandvan.co.uk/wp-content/uploads/2023/09/Isuzu-N75.190-E-Dropside-body-e1696502523616.png',
        'SUV': 'https://carassobucket1.s3.eu-west-1.amazonaws.com/austral_exterior_img_01_9d4cc1d1c7.png',
        'Van': 'https://www.toyota.com.sg/showroom/new-models/-/media/22809e987d75444cb099efa50ef7c26c.png',
        'Electric': 'https://www.tesla.com/ownersmanual/images/GUID-B5641257-9E85-404B-9667-4DA5FDF6D2E7-online-en-US.png',
    }
    return render_template("index.html", cars=cars, car_types=car_types, car_type_images=car_type_images)

cars_rented_counter = metrics.counter(
    'cars_rented_total',
    'Total number of cars rented'
)


@app.route("/rent", methods=["GET", "POST"])
def rent_car():
    """Process the form submission to rent a car."""
    if request.method == "POST":
        car_id = request.form["car_id"]
        selected_car = next((car for car in cars if car["brand"] == car_id), None)
        if selected_car:
            # Pass car_id to the user details template
            return render_template("user_details.html", car_id=car_id)
        return "Car not found. Received car_id: " + str(car_id)
    else:
        return "Method Not Allowed", 405


def generate_order_number():
    """Generate a unique order number."""
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=7))


@app.route("/confirm_rent", methods=["POST"])
def confirm_rent():
    """Process the form submission to confirm the car rental."""
    if request.method == "POST":
        print("Received form data:", request.form)  # Add this line for debugging
        car_id = request.form["car_id"]
        full_name = request.form["full_name"]
        license_number = request.form["license_number"]
        age = request.form["age"]
        start_date_str = request.form["start_date"]  # Assuming the date format is YYYY-MM-DD
        end_date_str = request.form["end_date"]

        start_date = datetime.strptime(start_date_str, "%Y-%m-%d")
        end_date = datetime.strptime(end_date_str, "%Y-%m-%d")

        # Calculate the number of rental days
        days_rented = (end_date - start_date).days

        selected_car = next((car for car in cars if car["brand"] == car_id), None)

        error_messages = []

        if not selected_car:
            return "Car not found. Received car_id: " + str(car_id)

        if not license_number.isdigit():
            error_messages.append("Invalid driving license number. Please enter only numbers.")

        if not age.isdigit():
            error_messages.append("Invalid age. Please enter only numbers.")
        elif int(age) < 24:
            error_messages.append("You are too young to rent a car.")
        elif int(age) > 90:
            error_messages.append("You are too old to rent a car.")

        if error_messages:
            return "\n".join(error_messages)

        total_cost = selected_car["price_per_day"] * days_rented
        brand = selected_car["brand"]
        year = selected_car["year"]
        order_number = generate_order_number()
        rental_info = {
            "full_name": full_name,
            "license_number": license_number,
            "age": age,
            "car_brand": brand,
            "car_model": selected_car["model"],
            "car_year": year,
            "start_date": start_date_str,
            "end_date": end_date_str,
            "days_rented": days_rented,
            "total_cost": total_cost,
            "order_number": order_number
        }
        rental_collection.insert_one(rental_info)

        message = (
            f"Thank you {full_name} for renting {brand} {selected_car['model']} ({year}). "
            f"Your total cost for {days_rented} days is ${total_cost}.\n"
            f"Your Order Number - {order_number}"
        )
        return render_template("confirmation_page.html", message=message)   
    else:
        return "Method Not Allowed", 405

@app.route("/add_driver", methods=["POST"])
def add_driver():
    """Add a new driver to the database."""
    full_name = request.json.get("driver_full_name")
    license_number = request.json.get("driver_license_number")
    age = request.json.get("driver_age")

    # Perform validation if necessary

    # Insert the data into the MongoDB collection
    driver_info = {
        "full_name": full_name,
        "license_number": license_number,
        "age": age
    }
    collection.insert_one(driver_info)

    # Return a JSON response indicating success
    return jsonify({"message": "Driver added successfully"})

@app.route('/save_driver_details', methods=['POST'])
def save_driver_details():
    if request.method == 'POST':
        # Process the form data here and save it to the database
        driver_full_name = request.form.get('driver_full_name')
        driver_license_number = request.form.get('driver_license_number')
        driver_age = request.form.get('driver_age')

        # Example: Save the data to MongoDB
        # mongo.db.reservation.insert_one({
        #     'driver_full_name': driver_full_name,
        #     'driver_license_number': driver_license_number,
        #     'driver_age': driver_age
        # })

        return 'Driver details saved successfully'
    else:
        return 'Method Not Allowed', 405

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
